﻿#ifndef DEF_ICE_MUSIC
#define DEF_ICE_MUSIC

void iceMusicCreate(char* path);
int iceMusicPlay(const int nb, const int volume);

#endif