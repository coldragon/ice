﻿#ifndef DEF_ICE_DEBUG
#define DEF_ICE_DEBUG

void iceDebugMouseCoordinate();
void iceDebugShowFps();
void iceDebugShowFpsTitle();
void iceDebugShowCollide();

#endif