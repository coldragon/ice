﻿#ifndef DEF_ICE_PHYSICS
#define DEF_ICE_PHYSICS

#include "TypesMaths.h"

iceBool icePhysicsBoxCollision(const iceBox box1, const iceBox box2);
void icePhysicsSetGravity(iceVect vect);

#endif
