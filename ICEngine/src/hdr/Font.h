#ifndef DEF_ICE_FONT
#define DEF_ICE_FONT

#include "Game.h"

void iceFontLoad(char *path);
void iceFontDraw(char* text, int size, iceVect vect);

#endif
