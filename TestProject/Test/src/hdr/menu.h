﻿#ifndef DEF_TEST_MENU
#define DEF_TEST_MENU
#include <Core.h>

void menu_create();
void menu_update();
void menu_destroy();

#endif